# Chrome Tab Counter

Google Chrome extension to count tabs

https://chromewebstore.google.com/detail/tab-counter/phojeebfjglippdniiejmfclojgbmedd

## Description

Shows you a count of the total number of tabs, total number of windows, and number of tabs in the current window.

Also has an option to export your tabs as a JSON file.
