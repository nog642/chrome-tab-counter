export function saveFile(contents, name, content_type) {
    const a = document.createElement("a");
    const blob_options = {};
    if (content_type !== undefined) {
        blob_options.type = content_type;
    }
    a.href = URL.createObjectURL(new Blob([contents], blob_options));
    a.download = name;
    a.click();
}
