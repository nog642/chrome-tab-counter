// background script

let tabCount = null;


chrome.tabs.onCreated.addListener(tab => {
    if (tabCount === null) {
        chrome.tabs.query({}, tabs => {
            tabCount = tabs.length;
            chrome.action.setBadgeText({"text": tabCount.toString()});
        });
    } else {
        tabCount++;
        chrome.action.setBadgeText({"text": tabCount.toString()});
    }
});


chrome.tabs.onRemoved.addListener(tab => {
    if (tabCount === null) {
        chrome.tabs.query({}, tabs => {
            tabCount = tabs.length;
            chrome.action.setBadgeText({"text": tabCount.toString()});
        });
    } else {
        tabCount--;
        chrome.action.setBadgeText({"text": tabCount.toString()});
    }
});


chrome.tabs.query({}, tabs => {
    tabCount = tabs.length;
    chrome.action.setBadgeText({"text": tabCount.toString()});
});
